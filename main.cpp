/*******************************************************************************
This is the main script of my brainfuck interpreter. It parses the source code,
preprocess the brackets position and than interprets it, mainly by calling the
Brain and Hand classes.
-Omniflex, 06/01/2019
*******************************************************************************/

#include <iostream>
#include <string.h>
#include <vector>
#include "parser.h"
#include "hand.h"
#include "brain.h"

using std::cout;
using std::endl;
using std::string;

int main(int argc, char* argv[])
{
  cout << endl;
  string path = argv[1];
  string raw_source = "";
  string source = "";
  string par = "()";
  string bra = "[]" ;
  string space = " ";

  raw_source = getSource(path); //import source from file as a string
  raw_source = deleteChar(raw_source, space); //deletes spaces in source
  cout << "Raw source : " << endl << raw_source << endl << endl;

  verifyMatch(raw_source, par); //checks is parentheses match
  source = deleteComments(raw_source); //deletes comments
  cout << "Parsed source : " << endl << source << endl << endl;
  verifyMatch(source, bra); //checks if brackets match
  verifyGrammar(source); //checks if every symbol is interpretable

  //Initialises Brain and Hand classes
  cout << "Linking Brain to Hand...";
  Brain brain(source);
  Hand hand;
  cout << "Done" << endl << endl;

  //Loop to deal with brackets
  cout << "Interpreting..." << endl;
  if (source.find('[') != std::string::npos)
  {
    int beg = source.find('[');
    while (source.find('[', beg) != std::string::npos)
    {
      beg = source.find('[', beg);
      brain.findCorresponding(beg);
      beg = brain.forwardBracket(beg);
    }
  }

  //Main loop
  while (brain.getPointer() < source.size())
  {
  brain.interpret(hand);
  }
  cout << "Done" << endl << endl;
  return 0;
}

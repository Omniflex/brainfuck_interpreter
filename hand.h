#ifndef HAND_H
#define HAND_H

#include <vector>

using std::vector;

class Brain;

class Hand
{
  friend class Brain;
  private:
    vector<int> array;
    int pointer;
  public:
    Hand();
    ~Hand();

    int getSize();

    int getValue();
    void incValue();
    void decValue();

    int getPointer();
    void setPointer(int);
    void incPointer();
    void decPointer();

    void output();
    void input();
};

#endif

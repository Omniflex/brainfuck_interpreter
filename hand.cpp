/*******************************************************************************
The Hand class takes care of all the read/write processez of the interpreter.
It is controlled by the Brain class in the main script. The array is a standard
vector while the pointer is its reading head.
-Omniflex, 06/01/2019
*******************************************************************************/

#include "hand.h"
#include "brain.h"
#include <vector>
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

//Constructor
Hand::Hand() : pointer(0), array(1, 0)
{}

//Destructor
Hand::~Hand()
{}

int Hand::getSize()
{return array.size();}

int Hand::getValue()
{return array.at(pointer);}

void Hand::incValue()
{array[pointer]++;}

void Hand::decValue()
{array[pointer]--;}

int Hand::getPointer()
{return pointer;}

void Hand::setPointer(int new_pointer)
{pointer = new_pointer;}

void Hand::output()
{cout << "OUTPUT : " << char(array[pointer]) << endl;}

void Hand::input()
{cout << "INPUT ? "; int inp; cin >> inp; array[pointer] = inp;}

void Hand::incPointer()
{
  pointer++;
  if (pointer >= array.size())
  {
    array.push_back(0);
  }
}

void Hand::decPointer()
{
  if (pointer == 0)
  {
    array.insert(array.begin(), 0);
  }
  else pointer--;
}

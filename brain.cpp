/*******************************************************************************
The Brain class does all the pre-processing and logical work of the interpreter.
Its main purpose is to control the Hand class which handles the read/write
processes.
It reads the source character by character, jumping when it must do so. The
reading head is denoted by the pointer variable
-Omniflex, 06/01/2019
*******************************************************************************/

#include "brain.h"
#include "hand.h"
#include <map>
#include <iostream>

using std::cout;
using std::endl;
using std::map;

//Constructor
Brain::Brain(string s) : pointer(0), source(s)
{}

//Destructor
Brain::~Brain()
{}

int Brain::getPointer()
{return pointer;}

void Brain::incPointer()
{pointer++;}

void Brain::setPointer(int new_pointer)
{pointer = new_pointer;}

void Brain::printSource()
{cout << source;}


//Create the mapping of matching brackets, called
//in a loop before the main interpretation loop
void Brain::findCorresponding(int beg)
{
  int stat = 1;
  int idx = beg+1;
  while (stat != 0 && idx < source.size())
  {
    if (source[idx]=='[')
    {
      stat++;
      findCorresponding(idx);
      idx++;
    }
    else if (source[idx]==']')
    {
      stat--;
      if (stat == 0)
      {
        forward[beg] = idx;
        backward[idx] = beg;
      }
      idx++;
    }
    else idx++;
  }
}

//Return the corresponding closing bracket. Used to pair every brackets.
int Brain::forwardBracket(int beg)
{
  return forward[beg];
}

//Main function to control the Hand
void Brain::interpret(Hand& hand)
{
  if (source[pointer] == '+')
  {hand.incValue(); incPointer();}

  else if (source[pointer] == '-')
  {hand.decValue(); incPointer();}

  else if (source[pointer] == '>')
  {hand.incPointer(); incPointer();}

  else if (source[pointer] == '<')
  {hand.decPointer(); incPointer();}

  else if (source[pointer] == '.')
  {hand.output(); incPointer();}

  else if (source[pointer] == ',')
  {hand.input(); incPointer();}

  else if (source[pointer] == '[')
  {
    if (hand.getValue() == 0) setPointer(forward[pointer]+1);
    else pointer++;
  }

  else if (source[pointer] == ']')
  {
    if (hand.getValue() != 0) setPointer(backward[pointer]+1);
    else pointer++;
  }

}

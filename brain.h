#ifndef BRAIN_H
#define BRAIN_H

#include <map>

using std::string;
using std::map;

class Hand;

class Brain
{
  friend class Hand;
  private:
    int pointer;
    string source;
    map<int,int> forward; //Map the position of an opening bracket to its corresponding one
    map<int, int> backward; //Inverse of the above function

  public:
    Brain(string);
    ~Brain();

    int forwardBracket(int);
    void findCorresponding(int);
    int getPointer();
    void incPointer();
    void setPointer(int);
    void interpret(Hand&);
    void printSource();
};
#endif

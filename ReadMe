[DESCRIPTION]
This project is a C++ implementation of a Brainfuck interpreter. It was primarily
a way for me to go back to this language and learn a lot of new things about it.
Took me around a day to complete.


[USAGE]
The a.out file is the binary and should be called this way in a shell :

$ ./a.out path/to/the/brainfuck/source/code

I implemented a comment parsing so everything that is between parentheses won't be
read by the interpreter. Be aware that nested comments will result in an error.

The Brainfuck is a really simple language written in only 8 symbols and is quite
challenging to program in. I've written a "Hello Chat!" program in Brainfuck
that you can use as an example. For more information, see :

wikipedia.org/wiki/Brainfuck


[IMPLEMENTATION]
The interpreter has 4 source files to it:

parser.cpp
hand.cpp
brain.cpp
main.cpp

The main file lauches the pipeline, the parser processes the brainfuck source
code, and the brain and hand work in tandem to interpret it. The brain does the
logic part of the process while the hand mainly reads and writes to the brainfuck
array. Each container of the array takes an int, and the array is itself a vector.
This is made so that you can go (almost) as far as you like in any direction, the
maximum size being around a billion components.

I chose to make the interpreter quite verbose in order to be able to debug
and see each step happening.


[IMPROVEMENTS]
The parser could benefit by a new way of suppressing comment, leading to the
ability to put nested comments. I know that my approach of the brainfuck interpreter
is neither the easiest one nor the most optimized one, but it is the one that
makes the most sense to me and allowed me to learn more about classes and their
interactions.


[MODIFICATIONS]
This project is under GNU GPL, so if you wish to modify the sources of the project,
be my guest :) Here are the bash commands needed to recompile everything properly :

$ g++ -c main.cpp parser.cpp brain.cpp hand.cpp
$ g++ main.o parser.o brain.o hand.o


[CONTACT]
omniflex@outlook.fr

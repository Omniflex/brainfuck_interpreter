/*******************************************************************************
THe parser is tasked to process the source code itself before feeding it to the
Brain and the Hand to interpret it. It performs lots of checks in order to make
sure that the Brain can interpret it. It mainly supresses comments and check
if delimiters match correctly. It can be improved by allowing nested comments
through a better deleteComments method.
-Omniflex, 06/01/2016
*******************************************************************************/

#include <iostream>
#include <string.h>
#include <fstream>
#include "parser.h"

using std::cout;
using std::endl;
using std::string;
using std::invalid_argument;
using std::ifstream;

//Check if the source contains different characters than "<>+-[].,"
void verifyGrammar(string source)
{
  int jmax = 8;
  char allowedChar[jmax] = "<>+-[].,";
  int i = 0;
  int j = 0;
  string compareStr = "";
  bool good = false;

  for (i = 0 ; i < source.length(); i++)
  {
    j = 0;
    good = false;
    while (good == false && j < jmax)
    {
      compareStr = allowedChar[j];
      if (source.substr(i,1).compare(compareStr) == 0) good = true;
      j++;
    }
    if (!good) throw invalid_argument("Found a non-authorized character in the source code");
  }
  cout << "Every character is in the accepted grammar" << endl << endl;
  return;
}

//Import the source into a string
string getSource(string path)
{
  string raw_source = "";
  string source = "";
  ifstream sourceCode;
  string line;
  sourceCode.open(path);
  if (sourceCode.is_open())
  {
    while ( getline (sourceCode,line) )
     {
       raw_source.append(line);
     }
     sourceCode.close();
  }
  else
  {
    throw invalid_argument("Unable to open source file");
  }
  return raw_source;
}

//Automatize the deletion of a particular substring in a string
string deleteChar(string s, string toDelete)
{
  while (s.find(toDelete) != std::string::npos)
  {
    s.erase(s.find(toDelete), toDelete.length());
  }
  return s;
}

//Deletes the comments. Can be improved by processing correctly the parenthesis
//matching, like the bracket one. For the time being, parentheses inside
//other enclosing parentheses will create troublesome output automatically
//rejected by the interpreter because it will still have a closing parenthesis.
string deleteComments(string s)
{
  string openChar = "(";
  string closeChar = ")";
  int open = 0;
  int close = 0;
  while (s.find(openChar) != std::string::npos)
  {
    open = s.find(openChar);
    close = s.find(closeChar);
    s.erase(open, close-open+1);
  }
  return s;
}

//Used to check if the opening and closing brackets and parentheses appear in
//same quantity.
bool verifyMatch(string s, string compChar)
{
  int stat = 0;
  int i = 0;

  for (i = 0; i < s.length(); i++)
  {
    if (s.substr(i,1).compare(compChar.substr(0,1)) == 0) stat++;
    if (s.substr(i,1).compare(compChar.substr(1,1)) == 0) stat--;
    if (stat < 0) throw invalid_argument("Unmatching delimiters");
  }

  if (stat == 0)
  {
    cout << "Delimiters "<< compChar <<" match correctly" << endl << endl;
    return true;
  }
  else {
    throw invalid_argument("Unmatching delimiters");
    return false;
  }
}

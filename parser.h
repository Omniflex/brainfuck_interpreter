using std::string;

string getSource(string);
string deleteChar(string, string);
string deleteComments(string);
bool verifyMatch(string, string);
void verifyGrammar(string);
